// 1. Дано: функція яка приймає масив чисел або стрічок.
// Результат: вивести у консоль масив унікальних значень початкового масиву
// Приклад: [2, 3, 1, 3, 3, 7] => [2,3,1,7]
const arrOfNumbers = [2, 3, 1, 3, 3, 7];

console.log(`Масив ункіальних з ${arrOfNumbers}:`);
const unique = (initialArray) => {
    return Array.from(new Set(initialArray));
}

console.log(unique(arrOfNumbers));

//2. Створити масив обєктів типу `{ firstName: ‘Jon’, lastName: ‘snow’ ratting: 100 }` для всіx одногрупників (оцінки взяти із попереднього курсу чи семестру за певний предмет) та написати методи для можливості:

const students = [
    { 
        firstName: 'Jon', 
        lastName: 'now', 
        ratting: 100 
    },
    { 
        firstName: 'Ivan', 
        lastName: 'Sytnyk', 
        ratting: 85 
    },
    { 
        firstName: 'Shawn', 
        lastName: 'Maddison', 
        ratting: 95 
    },
    { 
        firstName: 'John', 
        lastName: 'Snow', 
        ratting: 100 
    },
]

//  2.1 Вивести імена та прізвища одногрупників по алфавіту;

console.log("По алфавіту за ім'ям:");
const sortbyName = students.sort( ( a, b ) => { 
    const nameA = a.firstName.toUpperCase();
    const nameB = b.firstName.toUpperCase(); 

    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }
});

sortbyName.forEach( (el) => {
    console.log(el.firstName + " " + el.lastName);
})
console.log("По алфавіту за прізвищем:");
const sortBySecondName = students.sort( ( a, b ) => { 
    const nameA = a.lastName.toUpperCase();
    const nameB = b.lastName.toUpperCase(); 

    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }
});

sortBySecondName.forEach( (el) => {
    console.log(`${el.lastName} ${el.firstName}`);
})

//  2.2 Вивести імена та прізвища за оцінками

console.log("Посортовано за оцінками:");

const sorted = students.sort( ( a, b ) => {
    return b.ratting - a.ratting;
});

sorted.forEach(element => {
    console.log(element.firstName + " " + element.lastName + " " + element.ratting);
});

let max = 0;
let min = 100;

students.forEach((element) => {
    if ( element.ratting > max) {
        max = element.ratting;
    } 
    if ( element.ratting < min) {
        min = element.ratting;
    } 
});

console.log(max);
console.log(min);
//  2.3 Вивести ім’я та прізвище учня із максимальною, мінімальною та середньою оцінками.

students.forEach(element => {
    if (element.ratting == max) {
        console.log("Студент з найвищою оцінкою: " + element.firstName + " " + element.lastName);
    }
    if (element.ratting == min) {
        console.log("Студент з найнижчою оцінкою: " + element.firstName + " " + element.lastName);
    }
});
//  2.4 Створити новий масив на основі старого в якому буде додаткове поле 
//{ rate: 100 } , де rate на скільки відсотків учень відстав від лідера (найбільше значення поля ratting);

console.log("Список з полем rate:");

const newStudents = students.map( element => {
    element.rate = 100 - (max * element.ratting) /100;
});

students.forEach(( element, index ) => {
    console.log(`Student ${index + 1}: ` + element.firstName + " " + element.lastName + " ratting: " + element.ratting + " rate: " + element.rate);
});

//3. Створити метод який генерує рандомну строку пароль. Враховуючи можливість задати довжину, наявність чисел та символів. 

const randomPassword = ( len, num, sumb ) => {
    const pass = [];

    const symb = ['!','"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', ']','^', '_', '`', '{', '|', '}', '~', "\\"];
    const alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    const lowAlphabet = alphabet.map((element) => element.toLowerCase());
    const numb = [1,2,3,4,5,6,7,8,9,0]; 
    const allSym = [...alphabet, ...lowAlphabet];

    if (num == 'YES' && sumb == 'YES') {
        allSym.push(...symb);
        allSym.push(...numb);
    } else if (num == 'YES') {
        allSym.push(...numb);
    } else if (sumb == 'YES') {
        allSym.push(...symb);
    } 

    for (let i = 0; i < len; i++) {
        pass[i] = allSym[Math.floor(Math.random() * allSym.length)];
    } 

    return pass.join('');
}

console.log(randomPassword(10, 'YES', 'NO'));
